#include<stdio.h>
#include"emulate.h"
#include<pthread.h>
#include<stdlib.h>

/* populate the cmd line parameter in this structure */
s_cmd_line params;

/*pthread ID */
pthread_t token_threadId, transmitter_threadId, receiver_threadId;

/************************************************************
Function    : token_thread 
 
Description : thread that handles creating tokens and putting it in 
              output queue 

************************************************************/

void* token_thread()
{
    printf("token_thread created\n");
}

/************************************************************
Function    : transmitter_thread 
 
Description : thread that handles creating packts and putting it from 
              input queue to output queue

************************************************************/

void* transmitter_thread()
{
    printf("transmitter_thread created\n");
}

/************************************************************
Function    : receiver_thread 
 
Description : thread that recives packets an, processes and
              deletes packets from output queue

************************************************************/
void* receiver_thread()
{
    printf("receiver_thread created\n");
}

/************************************************************
Function    :parse_cmd_line 
 
Description : Parses command line and store the required
              arguments

************************************************************/

void parse_cmd_line(int argc, char *argv[])
{
	int i = 0; char *delim;
	for (i=1; i<argc; i+=2) // start from i=1, discard executable name
	{
                if(strcmp(argv[i],"-lambda") == 0)
		{	
			params.lambda = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-mu") == 0)
		{
			params.mu = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-rate") == 0)
		{
			params.rate = strtod(argv[i+1],&delim);
		}
		else if(strcmp(argv[i],"-depth") == 0)
		{
			params.depth = atoi(argv[i+1]);
		}
		else if(strcmp(argv[i],"-token") == 0)
		{
			params.tokens = atoi(argv[i+1]);
		}
                else
			printf("Wrong parameters\n");
	}
        printf(" Emulation User input Parameters \n");
        printf(" Arrival Time :: %.6g\n Service time of packet =%f\n rate at which tokens generated = %f\n token depth = %d\n number of tokens = %d\n",
			params.lambda,
			params.mu,
			params.rate,
			params.depth,
			params.tokens);

}
/************************************************************
Function    : main
 
Description : Parses command line, creates threads and queues

************************************************************/

int main(int argc,char *argv[])
{

   printf(" Emulate token bucket algorithm\n");
 
   //parse_cmd_line(argc,argv);
   
   //pthread creation
   int res = pthread_create(&token_threadId, NULL, &token_thread,NULL);
     if (res != 0)
     {
         printf("token thread creation failed\n");
     }
     res = pthread_create(&transmitter_threadId, NULL, &transmitter_thread,NULL);
     if (res != 0)
     {
         printf(" transmitter thread creation failed\n");
     }
     res = pthread_create(&receiver_threadId, NULL, &receiver_thread,NULL);
     if (res != 0)
     {
         printf("receiver thread creation failed\n");
     }
      pthread_join( token_threadId, NULL);
      pthread_join( transmitter_threadId, NULL);
      pthread_join( receiver_threadId, NULL);
   
   
   printf(" Token 1 arrived : Current token depth is 1\n");
   printf(" packet 1 have arrived\n");
   printf(" Token 2 arrived : Current token depth is 2\n");
   printf(" packet 2 have arrived\n");
   printf(" Token 3 arrived : Current token depth is 3\n");
   printf(" packet 3 have arrived\n");
   printf(" Token 4 arrived : Current token depth is 4\n");
   printf(" packet 4 have arrived\n");
   printf(" packet 1 sent to reciever\n");
   printf(" Token 5 arrived : Current token depth is 1\n");
   printf(" packet 2 sent to reciever\n");
   printf(" packet 3 sent to reciever\n");
   printf(" packet 4 sent to reciever\n");
   printf(" packet 5 sent to reciever\n");

   printf(" Token bucket Algorithm successfully emulated\n");
  
   printf(" \n\n Statistics\n 5 packets arrived with token depth of 4\n");
   printf(" Dropped packets : 0\n"); 

   return 1;

}
