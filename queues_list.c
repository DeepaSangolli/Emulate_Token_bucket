#include<stdio.h>
#include"emulate.h"
#include<stdbool.h>
#include<stdlib.h>


s_list *first = NULL;
s_list *last = NULL;

void add_item_list(s_list *queue,void *packet)
{
      /* Create a new element */
      s_list *newNode = (s_list *)malloc(sizeof(s_list));
      newNode->packet = packet;
      newNode->next = NULL;
      /* If the list is empty */
      if(!first)
      {
           first = newNode;
           last = newNode;
           
      }
      else
      {
           /* Add data at the last */
 	   last->next = newNode;
           last = newNode;
      }
}


void delete_item_list(s_list *queue,void *packet)
{

       /* if the list is empty */
	if(first == NULL)
		return;
        /* Item to be deleted is the first */
	if(first->packet == packet)
	{
              s_list *temp = first;
              first = first->next;
              free(temp);
	}
	else
	{
              s_list *current = first->next;
              s_list *trailCurrent = first;
              bool found = false;
              while(current != NULL && found != true) 
	      {
		      if(current->packet == packet)
		      {
			      found = true;
			      break;
		      }
                      trailCurrent = current; 
                      current = current->next; 
	      }
              
              if(found == true)
	      {
                  /* Item to be removed is the last element */
                  if(current == last)
                  {
                       last = trailCurrent;
                       last->next = NULL;
                       free(current);
		  }
		  else
		  {
                       trailCurrent->next = current->next;
                       free(current);
		  } 
	      }


	}


}

void print_all_elements()
{
     s_list *current = first;
     while(current != NULL)
     {
	     //printf("%d\n",current->data);
             current = current->next;
     }
}

