



typedef struct cmd_line{
	double lambda;  //interarrival time
	double mu;     //service time of the packet
	double rate;       //rate at which tokens are generated 
	int depth;           //token depth
	int tokens;            //number of tokens required for each packet
}s_cmd_line;


typedef struct list{
	void *packet;
	struct list *next;
}s_list;


/* Function Declarations */

void add_item_list(s_list *queue,void *packet);
void delete_item_list(s_list *queue,void *packet);
void print_all_elements();
